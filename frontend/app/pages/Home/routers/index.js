import React from 'react';
import HomePage from '../index';
import RouteLoaderPage from '../../../components/body/Loader';
import {Route} from 'react-router-dom';
import {GLOBAL} from '../../../config/index';

export default <Route exact  path = {GLOBAL.createPathLocale(HomePage.Path)}>
    <RouteLoaderPage>
        <HomePage/>
    </RouteLoaderPage>
</Route>;
