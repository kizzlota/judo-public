import '../allStyle/index.scss';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import BodyContainer from '../../components';
import {GLOBAL} from '../../config/index';
import User from '../../components/user/index';
import {PUBLICK_STATUS} from '../../const/PageInfo';
import * as Const from "../../const/PageInfo";


class HomePage extends Component {
    static Path = '/';
    static access_status = PUBLICK_STATUS;
    static animated_script = 'home';
    static pageName = 'home';

    constructor(props) {
        super(props);
    }

    setParamBody() {
        let bodyParam = ::this.getParamBody();
    }

    getCenterContent() {
        return (
            <div className="home-welcome">
                <header><h2>This is an open context page</h2></header>
                <section>
                    <p>WELCOME</p>
                </section>
            </div>
        );
    }

    getHeaderContent() {
        return <User/>;
    }

    static getInfo() {
        let {getLocalePage} = GLOBAL;
        let locale = getLocalePage('HomePageLocale');
        return {
            namePage: locale.header.title,
            value: HomePage.pageName
        };
    }

    getParamBody() {
        let {getParamsColumns} = GLOBAL;
        let headerParam = getParamsColumns('header');
        // let LeftContainerParam = getParamsColumns();
        let CenterContainerParam = getParamsColumns();
        // let RightContainerParam = getParamsColumns();
        let FooterParam = getParamsColumns('footer');
        headerParam.activeLink = HomePage.pageName;
        CenterContainerParam.child = ::this.getCenterContent();
        headerParam.child = ::this.getHeaderContent();
        FooterParam.activeLink = HomePage.pageName;
        return {
            headerParam,
            // LeftContainerParam,
            CenterContainerParam,
            // RightContainerParam,
            FooterParam
        };
    }

    componentDidMount() {
        let {dispatch} = this.props;
        let {changePageName} = GLOBAL;
        dispatch({type: Const.SWITCH_MENU, switcherMenu: true});
        if (!changePageName(dispatch, HomePage.pageName)) {
            console.error('error change pageName ' + HomePage.pageName);
        }
    }

    render() {
        let paramsBody = ::this.getParamBody();
        return (
            <div className="containerPage">
                <BodyContainer
                    {...paramsBody}
                />
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        locale: state.langPage.HomePageLocale
    };
}

export default connect(mapStateToProps)(HomePage);
