import './style/index.scss';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import BodyContainer from '../../components';
import PropTypes from 'prop-types';
import is from 'is_js';
import {GLOBAL} from '../../config/index';
import {PROTECTED_STATUS} from '../../const/PageInfo';
import User from '../../components/user/index';
import * as Const from '../../const/PageInfo';


class AboutPage extends Component {
    static Path = '/about';
    static access_status = PROTECTED_STATUS;
    static animated_script = 'home';
    static pageName = 'about';

    constructor(props) {
        super(props);
        let state = {
            showMenu: false
        };
        this.state = state;
    }
    componentWillMount() {
        let {dispatch} = this.props;
        let {localeSwitch, getPathForUrl, changePageName} = GLOBAL;
        let lang = getPathForUrl('lang');
        if (!localeSwitch(dispatch, lang)) {
            console.error('error change localeSwitch ' + AboutPage.pageName);
        }
        if (!changePageName(dispatch, AboutPage.pageName)) {
            console.error('error change pageName ' + AboutPage.pageName);
        }
    }

    static getInfo() {
        let {getLocalePage} = GLOBAL;
        let locale = getLocalePage('AboutPageLocale');
        return {
            namePage: locale.header.title,
            value: 'about'
        };
    }

    shouldComponentUpdate(nextProps, nextState) {
        return this.props.switcherMenu != nextProps.switcherMenu;
    }

    getHeaderContent() {
        return <User/>;
    }
    componentDidMount() {
        let {dispatch} = this.props;
        dispatch({type: Const.SWITCH_MENU, switcherMenu: true});
    }
    getParamBody() {
        let {getParamsColumns} = GLOBAL;
        let headerParam = getParamsColumns('header');
        let LeftContainerParam = getParamsColumns();
        let CenterContainerParam = getParamsColumns();
        let RightContainerParam = getParamsColumns();
        let FooterParam = getParamsColumns('footer');
        headerParam.activeLink = this.pageName;
        headerParam.child = ::this.getHeaderContent();
        // CenterContainerParam.child = ::this.getCenterContent();
        FooterParam.activeLink = this.pageName;
        return {
            headerParam,
            LeftContainerParam,
            CenterContainerParam,
            RightContainerParam,
            FooterParam
        };
    }

    render() {
        let paramsBody = ::this.getParamBody();
        let {locale} = this.props;
        if (is.undefined(locale) || !locale) {
            let {getLocalePage} = GLOBAL;
            locale = getLocalePage('AboutPageLocale');
        }
        return (
            <div className="containerPage animated fadeIn">
                <BodyContainer
                    {...paramsBody}
                />
            </div>
        );
    }
}

AboutPage.PropTypes = {
    listMenu: PropTypes.array.isRequired
};

function mapStateToProps(state) {
    return {
        locale: state.langPage.AboutPageLocale,
        switcherMenu: state.switcherMenu
    };
}

export default connect(mapStateToProps)(AboutPage);
