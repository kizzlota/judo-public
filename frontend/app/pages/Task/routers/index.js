import React from 'react';
import TodosPage from '../index';
import {Route} from 'react-router-dom';
import {GLOBAL} from '../../../config/index';
import RouteLoaderPage from '../../../components/body/Loader';

export default<Route exact path={GLOBAL.createPathLocale(TodosPage.Path)}>
    <RouteLoaderPage><TodosPage/></RouteLoaderPage>
</Route>;
