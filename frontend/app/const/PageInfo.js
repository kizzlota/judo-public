export const CHANGE_PAGE = 'PAGE_INFO';// change the name of the active page
export const CHANGE_LANG = 'CHANGE_LANGUAGE';// change language
export const ADD_CONTENT_LANG = 'ADD_CONTENT_LANGUAGE';// add localization the pages in store
export const SWITCH_MENU = 'SWITCH_MENU';// animation change pages
export const OPEN_STATUS = 'OPEN_STATUS';// user login
export const USER_INFO = 'USER_INFO';// user information after login
export const LOGIN_STATUS = 'LOGIN_STATUS';// login form status
export const AUTH_TRUE = 'AUTH_TRUE';
export const TASKS_INFO = 'TASKS_INFO';
export const TASKS_INFO_ACTIVE = 'TASKS_INFO_ACTIVE';
export const UPDATE_TASK = 'UPDATE_TASK';
export const ADD_TASK = 'ADD_TASK';
export const DELL_TASK = 'DELL_TASK';


export const PUBLICK_STATUS = 'PUBLICK';
export const PROTECTED_STATUS = 'PRTECTED';

export const COOCKIE_TIME_LIVE = 3600;
