const ssl = false;
const port = 3031;
const domain = 'localhost';
const protocol = ssl ? 'https://' : 'http://';
export let full_domain = port ? `${domain}:${port}` : domain;

let prefix = `${protocol}${full_domain}`;

export let api_user_token = `${prefix}/api-token-auth/`;
// export let checkToken = `${prefix}/api/token-check/`;
export let checkToken = `${prefix}/api/account/token-check/`;


//tasks

export let all_tasks_get = `${prefix}/api/task/all/`;
export let tasks_id_update = (id) => `${prefix}/api/task/update/${id}/`;
export let retrieve_tasks_id_post = (id) => `${prefix}/api/task/retrieve/${id}/`;
export let create_tasks_post = `${prefix}/api/task/create/`;
export let tasks_id_delete = (id) => `${prefix}/api/task/delete/${id}/`;
