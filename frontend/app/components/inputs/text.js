/* eslint-disable camelcase */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import is from 'is_js';


class InputText extends Component {
    allowed_types = ['text', 'email', 'password'];
    changed = false;

    constructor(props) {
        super(props);
        this.state = {
            value: ''
        };
        this.inpetData = this.inpetData.bind(this);
    }

    generate_id(type = 'text') {
        return `input_${type}_id_${new Date().getTime()}`;
    }

    inpetData(event) {
        if (!this.changed) this.changed = true;
        this.setState({value: event.target.value});
    }

    render() {
        let {
            placeholder = '',
            valueDefault = '',
            id = '',
            addClass = '',
            type = 'texts',
            label = '',
            icon = '',
            required = '',
            disabled = ''
        } = this.props;
        let {value = valueDefault} = this.state;
        if (valueDefault && !this.changed) {
            value = valueDefault;
        }
        type = is.inArray(type, this.allowed_types) ? type : 'text';
        id = is.not.empty(id) ? id : ::this.generate_id(type);
        let inputParams = {
            id,
            type,
            placeholder,
            value,
            onChange: this.inpetData
        };
        if (is.not.empty(required)) {
            inputParams.required = true;
        }
        if (is.not.empty(disabled)) {
            inputParams.disabled = disabled;
        }

        icon = is.not.empty(icon) ? icon : 'fi-pencil small';
        return (
            <div
                className='input-group'
            >
                <table>
                    <tbody>
                        <tr>
                            <td>
                                {is.not.empty(label)
                                    ?
                                    <label
                                        htmlFor={id}
                                    >
                                        {label}
                                    </label>
                                    :
                                    null
                                }
                            </td>
                            <td>
                                <input
                                    className={`input-type${is.not.empty(addClass) ? ` ${addClass}` : ''}`}
                                    {...inputParams}
                                />
                            </td>
                            <td>
                                <i className={icon}/>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        locale: state.langPage.inputsElements.inputText
    };
}

export default connect(mapStateToProps)(InputText);
