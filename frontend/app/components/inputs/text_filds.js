import React, {Component} from 'react';
import is from 'is_js';


export default class InpytTextFilds extends Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if (this.nameInput)
            this.nameInput.focus();
    }

    render() {
        let {addClass = '', onChange, disabled = false, value = '', placeholder = '', required = false, autofocus = false, type = 'text'} = this.props;
        let params = {className: 'input-fild-task', type};
        if (onChange) {
            params.onChange = onChange;
        }
        if (is.not.empty(addClass)) {
            params.className += ` ${addClass}`;
        }
        if (is.string(value)) {
            params.value = value;
        }
        if (disabled) {
            params.disabled = disabled;
        }
        if (placeholder) {
            params.placeholder = placeholder;
        }
        if (required) {
            params.required = required;
        }
        return (
            type !== 'textarea'
                ?
                <input
                    ref={(input) => {
                        autofocus ? this.nameInput = input : null;}}
                    {...params}
                />
                :
                <textarea
                    ref={(input) => {autofocus ? this.nameInput = input : null;}}
                    {...params}
                />
        );
    }
}
