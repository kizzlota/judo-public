/* eslint-disable consistent-return */
import React, {Component} from 'react';
import './style/index.scss';
import '../../sass/fonts/_foundation.scss';
import CenterContainer from './body/CenterColumn';
import LeftContainer from './body/LeftColumn';
import RightContainer from './body/RightColumn';
import Header from './body/header';
import Footer from './body/Footer';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import * as Const from '../const/PageInfo';
import Login from './loginForm';

class BodyContainer extends Component {
    constructor(props) {
        super(props);
        this.changePage = this.changePage.bind(this);
    }

    changePage() {
        let {dispatch, switcherMenu} = this.props;
        dispatch({type: Const.SWITCH_MENU, switcherMenu: !switcherMenu});
    }

    render() {
        let {
            headerParam,
            LeftContainerParam,
            CenterContainerParam,
            RightContainerParam,
            FooterParam,
            pageInfo
        } = this.props;
        let {visability: visabilityLeft} = LeftContainerParam;
        let {visability: visabilityRight} = RightContainerParam;
        let {visability: visabilityHeader} = headerParam;
        let {visability: visabilityFooter} = FooterParam;
        let proportionsCentr = (!visabilityLeft && !visabilityRight) ? '_full' : (!visabilityLeft || !visabilityRight ? '_plus_one_column' : '');
        let proportionsVerticall =
            (!visabilityHeader && !visabilityFooter)
                ? '_full'
                :
                (!visabilityFooter
                    ?
                    '_plus_one_row_footer'
                    :
                    (!visabilityHeader
                        ?
                        '_plus_one_row_header'
                        :
                        ''
                    )
                );
        CenterContainerParam.proportions = proportionsCentr;
        CenterContainerParam.proportionsVertical = proportionsVerticall;
        LeftContainerParam.proportionsVertical = proportionsVerticall;
        RightContainerParam.proportionsVertical = proportionsVerticall;
        return (
            <div id="bodyContainer">
                <Login/>
                <Header
                    {
                    ...{
                        changePage: this.changePage,
                        pageInfo,
                        ...headerParam
                    }
                    }
                />
                <LeftContainer
                    {...LeftContainerParam}
                />
                <CenterContainer
                    {...CenterContainerParam}
                />
                <RightContainer
                    {...RightContainerParam}
                />
                <Footer
                    {...FooterParam}
                />
            </div>
        );
    }
}


BodyContainer.defaultProps = {
    headerParam: {
        visability: false
    },
    LeftContainerParam: {
        visability: false
    },
    CenterContainerParam: {
        visability: false
    },
    RightContainerParam: {
        visability: false
    },
    FooterParam: {
        visability: false
    }
};

BodyContainer.PropTypes = {
    headerParam: PropTypes.object.isRequired,
    LeftContainerParam: PropTypes.object.isRequired,
    CenterContainerParam: PropTypes.object.isRequired,
    RightContainerParam: PropTypes.object.isRequired,
    FooterParam: PropTypes.object.isRequired
};

function mapStateToProps(state) {
    return {
        switcherMenu: state.switcherMenu,
        pageInfo: state.pageInfo
    };
}

export default connect(mapStateToProps)(BodyContainer);
