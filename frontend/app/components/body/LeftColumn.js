import React, {Component} from 'react';
import PropTypes from 'prop-types';
import is from 'is_js';
import {GLOBAL} from '../../config';

export default class LeftContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {height: props.height};
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        let {buildBodyCenterHeight} = GLOBAL;
        this.setState({height: buildBodyCenterHeight()});
    }

    render() {
        let {visability, children, child, proportionsVertical, class_add = ''} = this.props;
        let {height} = this.state;
        return (
            visability
                ?
                <div
                    id="LeftContainer"
                    style={{height}}
                    className={(is.not.empty(proportionsVertical) ? `vert_size${proportionsVertical}` : '') + (is.not.empty(class_add) ? ` ${class_add}` : '')}
                >
                    {child}
                    {children}
                </div>
                :
                null
        );
    }
}

LeftContainer.defaultProps = {
    visability: false,
    child: null
};


LeftContainer.PropTypes = {
    visability: PropTypes.bool.isRequired,
    child: PropTypes.object.isRequired
};

