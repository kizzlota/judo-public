import React, { Component } from 'react';

export default class Footer extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        let {visability} = this.props;
        return (
            visability
                ?
                <footer id="footer"/>
                :
                null
        );
    }
}
