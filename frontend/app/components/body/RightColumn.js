import React, {Component} from 'react';
import PropTypes from 'prop-types';
import is from 'is_js';
import {GLOBAL} from "../../config";

export default class RightContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {height: props.height};
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        let {buildBodyCenterHeight} = GLOBAL;
        this.setState({height: buildBodyCenterHeight()});
    }

    render() {
        let {visability, child, proportionsVertical} = this.props;
        let {height} = this.state;
        return (
            visability
                ?
                <div
                    id="RightContainer"
                    style={{height}}
                    className={(is.not.empty(proportionsVertical) ? `vert_size${proportionsVertical}` : '')}
                >
                    <h1>RightContainer</h1>
                    {child}
                </div>
                :
                null
        );
    }
}

RightContainer.defaultProps = {
    visability: false,
    child: null
};


RightContainer.PropTypes = {
    visability: PropTypes.bool.isRequired,
    child: PropTypes.object.isRequired
};
