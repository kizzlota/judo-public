import React, {Component} from 'react';
import PropTypes from 'prop-types';
import logo from '../../../../static/img/logo/judo-script-logo.png';

export default class Logo extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div className="logoContainer">
                <img
                    className='logo animated logo_rotate'
                    src={logo}
                    alt="logo"/>
            </div>
        );
    }
}


Logo.PropTypes = {
    visability: PropTypes.bool.isRequired, // відображення
    icon: PropTypes.string.isRequired// іконка
};
