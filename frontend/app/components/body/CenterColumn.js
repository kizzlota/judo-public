import React, {Component} from 'react';
import PropTypes from 'prop-types';
import is from 'is_js';
import {GLOBAL} from '../../config';

export default class CenterContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {height: props.height};
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        let {buildBodyCenterHeight} = GLOBAL;
        this.setState({height: buildBodyCenterHeight()});
    }

    render() {
        let {visability, child, proportions, proportionsVertical, children} = this.props;
        let {height} = this.state;
        return (
            visability
                ?
                <div
                    id="CenterContainer"
                    style={{height}}
                    className={`centr_size${proportions}${(is.not.empty(proportionsVertical) ? ` vert_size${proportionsVertical}` : '')}`}
                >
                    {child}
                    {children}
                </div>
                :
                null
        );
    }
}

CenterContainer.defaultProps = {
    visability: false,
    child: null
};


CenterContainer.PropTypes = {
    visability: PropTypes.bool.isRequired,
    child: PropTypes.object.isRequired
};
