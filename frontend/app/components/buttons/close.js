import React from 'react';
import is from 'is_js';

let ButtonClose = (dispatch, action = {}, addClass = '') => {
    return (
        <div
            className={`bottom-small-close${is.not.empty(addClass) ? ` ${addClass}` : ''}`}
            onClick={is.function(dispatch) && (() => dispatch(action))}
        >
            <i className="fi-x medium"/>
        </div>
    );
};

export default ButtonClose;
