import React, {Component} from 'react';
import {connect} from 'react-redux';
import ButtonClose from '../buttons/close';
import ButtonSubmit from '../buttons/submit';
import {LOGIN_STATUS} from '../../const/PageInfo';
import InputText from '../inputs';
import is from 'is_js';
import {GLOBAL} from '../../config/index';
import {openForm} from './actions';
import {authSuccess} from '../user/actions';
import {api_user_token} from '../../urls';

let {Post} = GLOBAL;

class Login extends Component {
    indicator_opened = false;
    stop = false;

    constructor(props) {
        super(props);
        this.state = {
            warning: false,
            open_status: false,
            error: '',
            block: false
        };
    }


    warningOpenForm(e) {
        let mask_id = e.target.id;
        let {statusFormLogin} = this.props;
        if (statusFormLogin && mask_id == 'form-login-mask') {
            let _this = this;
            let warning = () => setTimeout(() => _this.setState({warning: false}), 1000);
            this.setState({warning: true}, warning);
        }
    }

    closeForm(anim_class) {
        let {open_status} = this.state;
        let {statusFormLogin} = this.props;
        if (this.indicator_opened) {
            this.indicator_opened = false;
            let _this = this;
            if (open_status || !statusFormLogin)
                if (this.stop) {
                    clearTimeout(this.stop);
                    this.stop = false;
                }
            this.stop = setTimeout(() => _this.setState({open_status: !open_status, block: false}), 1000);
            return (
                ::this.getForm(anim_class)
            );
        }
        return null;
    }

    async submitForm(event) {
        event.stopPropagation();
        event.preventDefault();
        let {locale, dispatch} = this.props;
        let inputs = event.target.querySelectorAll('input');
        let params = {username: '', password: ''};
        let _this = this;
        this.setState({block: true});
        let clear_err = () => setTimeout(() => _this.setState({error: '', block: false}), 3000);
        if (is.not.empty(inputs)) {
            for (let i = 0, length_inp = inputs.length; i < length_inp; i++) {
                let input = inputs[i];
                let type_input = input.getAttribute('type');
                if (is.inArray(type_input, ['text', 'email'])) {
                    params.username = input.value;
                } else if (type_input === 'password') {
                    params.password = input.value;
                } else {
                    continue;
                }
            }
            if (is.empty(params.username) || is.empty(params.password)) {
                this.setState({error: locale.error_auth}, clear_err);
            } else {
                let res = await Post(api_user_token, params, dispatch);
                let {data, status} = res;
                if (status !== 200) {
                    let {ERROR} = data;
                    let error = is.not.empty(ERROR) ? ERROR : locale.error_default;
                    this.setState({error, block: false}, clear_err);
                } else if (status === 200) {
                    let {updateToken} = GLOBAL;
                    let {token} = data;
                    if (is.not.empty(token) && is.not.undefined(token)) {
                        updateToken(token);
                    }
                    this.setState({block: false});
                    dispatch(openForm(false));
                    dispatch(authSuccess({statusOpen: true, userInfo: {email: params.username}}));
                } else {
                    this.setState({error: '', block: false}, clear_err);
                }
            }
        } else {
            this.setState({error: locale.error_form}, clear_err);
        }
    }

    componentWillUnmount() {
        if (this.stop) {
            clearTimeout(this.stop);
            this.stop = false;
        }
    }

    getForm(animClass) {
        let {warning, error, block} = this.state;
        let {dispatch, statusFormLogin, locale} = this.props;
        if (statusFormLogin) {
            this.indicator_opened = statusFormLogin;
        }
        let action_close = {type: LOGIN_STATUS, statusFormLogin: false};
        let Close = ButtonClose(dispatch, action_close, 'right-top-abs');
        return (
            <div
                id="form-login-mask"
                className={`login-mask ${animClass} animated`}
                onClick={::this.warningOpenForm}
            >
                <div
                    className={`login-form${warning ? ' animated shake' : ''}`}
                >
                    <header>
                        <h2>{locale.title}</h2>
                        {Close}
                    </header>
                    <section>
                        <form
                            method='post'
                            onSubmit={::this.submitForm}
                        >
                            <InputText
                                required={true}
                                disable={block}
                                addClass="testwe"
                                type='email'
                                placeholder='email'
                                valueDefault=""
                                label='email'
                            />
                            <InputText
                                required={true}
                                disable={block}
                                addClass="test"
                                type='password'
                                placeholder='pass'
                                valueDefault=""
                                label='pass'
                                icon="fi-key small"
                            />
                            {ButtonSubmit(locale.login, block, 'button-medium')}
                            <br/>
                            {
                                is.not.empty(error)
                                    ?
                                    <div
                                        className="error-container animated flipInX"
                                    >
                                        {locale.error_auth}
                                    </div>
                                    :
                                    null
                            }
                        </form>
                    </section>
                </div>
            </div>);
    }

    render() {
        let {statusFormLogin} = this.props;
        let anim_class = statusFormLogin ? 'bounceInUp' : 'bounceOutDown';
        return (
            statusFormLogin && !this.indicator_opened
                ?
                ::this.getForm(anim_class)
                :
                ::this.closeForm(anim_class)
        );
    }
}

function mapStateToProps(state) {
    return {
        locale: state.langPage.loginForm,
        statusFormLogin: state.statusFormLogin
    };
}

export default connect(mapStateToProps)(Login);
