import React, {Component} from 'react';
import {connect} from 'react-redux';
import {GLOBAL} from '../../config';
import {all_tasks_get} from '../../urls';
import TaskElement from './taskElement';
import is from 'is_js';
import {taskData} from './actions';

class TascContainer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tasks: null,
            load: true
        };
    }


    createListTask(data = []) {
        let {locale, tasks: {active_task = null}, dispatch} = this.props;
        let tasksList = [];
        if (is.not.empty(data)) {
            for (let i = 0, len = data.length; i < len; i++) {
                let {id, name, status, created, description, first_name} = data[i];
                let activeClass = false;
                if (active_task) {
                    let {id: id_active} = active_task;
                    if (is.equal(~id, ~id_active)) {
                        activeClass = 'active-li';
                    }
                }
                tasksList.push(
                    <TaskElement
                        {...{id, name, status, created, description, first_name, locale, activeClass, dispatch}}
                    />
                );
            }
            return (<ul className="task-container-list">{tasksList}</ul>);
        }
    }


    async getTasks() {
        let {dispatch, statusOpen} = this.props;
        let {Get} = GLOBAL;
        let res = Get(all_tasks_get, {}, dispatch, statusOpen);
        let result = await res;
        let {data, status} = result;
        if (status >= 200 && status < 300) {
            dispatch(taskData(data));
        }  else {
            console.log('ERROR');
        }
    }

    componentDidMount() {
        ::this.getTasks();
    }

    render() {
        let {locale, tasks} = this.props;
        let tasks_list = ::this.createListTask(tasks.data);


        return (
            <div
                className="task-container-body"
            >
                <header>
                    <h3>{locale.title}</h3>
                </header>
                <section>
                    <div
                        className="task-container"
                    >
                        {tasks_list}
                    </div>
                </section>
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        locale: state.langPage.taskLocale,
        tasks: state.Tasks,
        statusOpen: state.statusOpen
    };
}

export default connect(mapStateToProps)(TascContainer);
