import {TASKS_INFO, TASKS_INFO_ACTIVE, UPDATE_TASK, ADD_TASK, DELL_TASK} from '../../../const/PageInfo';
import is from 'is_js';

let initialState = {data: [], active_task: {}};


function statusTask(state = initialState, action) {
    switch (action.type) {
        case TASKS_INFO: {
            let {active_task} = state;
            let {tasks = []} = action;
            tasks = (is.array(tasks) && tasks.length > 0) ? tasks : [];
            let newState = {...state, data: tasks};
            if (is.empty(active_task) && is.array(tasks) && tasks.length > 0) {
                newState.active_task = tasks[0];
            }
            return newState;
        }
        case TASKS_INFO_ACTIVE: {
            let {id = 0} = action;
            let {data} = state;
            let data_active = is.not.empty(data) && data.length > 0 ? data.filter((el) => is.equal(~el.id, ~id)).pop() : {};
            return {...state, active_task: data_active};
        }
        case UPDATE_TASK: {
            let {id = 0, new_data = {}} = action;
            let {data} = state;
            let data_active = is.not.empty(data) && data.length > 0
                ? data.map(
                    (el, index) => {
                        if (is.equal(~el.id, ~id)) {
                            return new_data;
                        }
                        return el;
                    }
                )
                :
                {};
            return {...state, data: data_active, active_task: new_data};
        }
        case DELL_TASK: {
            let {id = 0} = action;
            let {data} = state;
            let data_active = is.not.empty(data) && data.length > 0 ? data.filter((el) => is.not.equal(~el.id, ~id)) : [];
            return {...state, data: data_active, active_task: is.not.empty(data_active, data_active.keys().length > 0) ? data_active[0] : {}};
        }
        case ADD_TASK: {
            let {new_data = {}} = action;
            let {data = []} = state;
            let data_modif = data.concat(new_data);
            return {...state, data: data_modif, active_task: new_data};
        }
        default:
            return state;
    }
}

export default statusTask;
