import React, {Component} from 'react';
import {connect} from 'react-redux';
import {GLOBAL} from '../../config';
import {tasks_id_update, create_tasks_post, tasks_id_delete} from '../../urls';
import {updateTask, deleteTask, addTask} from './actions';
import InpytTextFilds from '../inputs/text_filds';
import is from 'is_js';
import ButtonClick from '../buttons/click';
import ButtonSubmit from '../buttons/submit';
import loader from '../../../static/img/show_loader.gif';


class TaskInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
            load: false,
            change: false,
            add: false,
            dell: false,
            description: '',
            name: '',
            status: '',
            id: ''
        };
        this.createValue = this.createValue.bind(this);
    }

    async updateInfo(event) {
        if (event) {
            event.stopPropagation();
            event.preventDefault();
        }

        let {dispatch, locale, statusOpen} = this.props;
        let {Update, Delete, Post} = GLOBAL;
        let {name, description, id, status, change, dell, add} = this.state;
        if (change) {
            this.setState({load: true});
            let url_update = tasks_id_update(id);
            let res = Update(url_update, {name, description, id, status}, dispatch, statusOpen);
            let result = await res;
            let {data, status: respons_status} = result;
            if (respons_status === 200) {
                dispatch(updateTask(id, data));
            } else {
                this.setState({load: false});
            }
        } else if (add) {
            this.setState({load: true});
            status = is.empty(status) ? 'open' : status;
            let res = Post(create_tasks_post, {name, description, status}, dispatch, statusOpen);
            let result = await res;
            let {data, status: respons_status} = result;
            if (respons_status === 201) {
                dispatch(addTask(data));
            } else {
                this.setState({load: false});
            }
        } else if (dell) {
            let url_delete = tasks_id_delete(id);
            let warning = locale.delete_confirm.format(`"${name}"`);
            let confirmation = confirm(warning);
            if (confirmation) {
                let res = Delete(url_delete, {id}, dispatch, statusOpen);
                let result = await res;
                let {status: respons_status} = result;
                if (respons_status === 200) {
                    dispatch(deleteTask(id));
                } else {
                    this.setState({load: false});
                }
            } else {
                ::this.clearState();
            }
        }
    }

    statusChange(e) {
        let {active_task: {id = ''}} = this.props;
        this.setState({status: e.target.value, id});
    }

    onChange(field, e) {
        let {active_task: {id = ''}} = this.props;
        let {add} = this.state;
        let value = e.target.value;
        id = !add ? id : '';
        this.setState({[field]: value, id, firstOpenAdd: false});
    }


    componentDidUpdate() {
        let {dell} = this.state;
        if (dell) {
            ::this.updateInfo();
        }
    }

    createValue(field) {
        let {change, add} = this.state;
        let {active_task = {}} = this.props;
        let value_field = this.state[field];
        let default_val = is.not.empty(value_field) || add ? value_field : active_task[field];
        let element = is.not.empty(value_field) && !add ? value_field : default_val;
        if (change || add) {
            if (field !== 'status') {
                element =
                    <InpytTextFilds
                        addClass="animated fadeIn"
                        value={element}
                        required={field === 'name' && add}
                        autofocus={field === 'name'}
                        placeholder={field}
                        type={field === 'name' ? 'text' : 'textarea'}
                        onChange={this.onChange.bind(this, field)}
                    />;
            } else {
                let status_list = ['open', 'done'];
                element =
                    <select
                        className="animated fadeIn"
                        onChange={this.onChange.bind(this, field)}
                        value={element}
                    >
                        {
                            status_list.map((elem, index) => {
                                return (
                                    <option
                                        key={index}
                                        value={elem}
                                    >
                                        {elem}
                                    </option>);
                            })
                        }
                    </select>;
            }
        }
        return element;
    }

    clearState() {
        this.setState({
            change: false,
            add: false,
            load: false,
            dell: false,
            description: '',
            name: '',
            status: '',
            id: ''
        });
    }

    componentWillReceiveProps(nextProps) {
        let {active_task: old_task} = this.props;
        let {active_task} = nextProps;
        if (old_task !== active_task) {
            ::this.clearState();
        }
    }


    render() {
        let {active_task = {}, locale} = this.props;
        let {change, add, dell, load} = this.state;
        let {created, first_name, name, description, id, status} = active_task;
        let {getDate} = GLOBAL;
        let visible_button_success = change || add;
        let But_cancel = ButtonClick(::this.setState, {
            add: false,
            dell: false,
            change: false,
            name: '',
            description: '',
            id: ''
        }, locale.cancel, false, 'fi-x', 'cancel-task');

        let But_add = ButtonClick(::this.setState, {
            add: true,
            dell: false,
            change: false,
            name: '',
            description: '',
            id: ''
        }, '', (dell || change || load), 'fi-plus small', 'add-task');

        let But_edit = ButtonClick(::this.setState, {
            change: true,
            name,
            description,
            id,
            status,
            dell: false,
            add: false
        }, '', (add || dell || load), 'fi-page-edit small', 'change-task');

        let But_delete = ButtonClick(::this.setState, {
            dell: true,
            firstOpenAdd: false,
            id,
            name,
            add: false,
            change: false
        }, '', (add || change || load), 'fi-trash small', 'dell-task');

        return (
            <div
                className="task-info-container  animated fadeIn"
            >
                <div
                    className="control-panel-task"
                >
                    <table>
                        <tbody>
                            <tr>
                                <td>{But_add}</td>
                                <td>{is.not.empty(active_task) && But_edit}</td>
                                <td>{is.not.empty(active_task) && But_delete}</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                {
                    is.not.empty(active_task) || add
                        ?
                        <div>
                            <form
                                onSubmit={::this.updateInfo}
                            >
                                {
                                    load ? <div className="mask-loader"><img src={loader}/></div> : null
                                }
                                <header>{<h2>{this.createValue('name')}</h2>}</header>
                                <section>
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>{locale.description}:</td>
                                                <td>{this.createValue('description')}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </section>
                                <section>
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>{locale.first_name}:</td>
                                                <td>{first_name}</td>
                                            </tr>
                                            <tr>
                                                <td>{locale.status}:</td>
                                                <td>{::this.createValue('status')}</td>
                                            </tr>
                                            <tr>
                                                <td>{locale.created}:</td>
                                                <td>{getDate(!add ? created : '')}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </section>
                                <section>
                                    <table>
                                        <tbody>
                                            <tr>
                                                <td>{visible_button_success && But_cancel}</td>
                                                <td>{visible_button_success && ButtonSubmit(locale.success, false, 'button-medium task-success animated fadeIn')}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </section>
                            </form>
                        </div>
                        :
                        <h2>{locale.info_empty}</h2>
                }
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        locale: state.langPage.taskLocale,
        active_task: state.Tasks.active_task,
        statusOpen: state.statusOpen
    };
}

export default connect(mapStateToProps)(TaskInfo);


