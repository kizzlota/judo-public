import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {changeInfo} from './actions';


export default class TaskElement extends Component {
    constructor(props) {
        super(props);
    }

    changeTackScreen() {
        let {id, dispatch} = this.props;
        dispatch(changeInfo(id));
    }

    render() {
        let {id, status, name, description, created, first_name, locale, activeClass} = this.props;
        let {first_name: author, name: discr_name, status: discr_status, description: discr_description} = locale;
        return (
            <li
                id={`task_${id}`}
                className={activeClass ? activeClass : ''}
                onClick={::this.changeTackScreen}
            >
                <h2>{name}</h2>
                <div className="discript-box">{description}</div>
                <p><span className="mark">{discr_status}:</span><span
                    className={`status-open${` ${status}_status`}`}>{status}</span></p>
                <p><span className="mark">{author}:</span><span className="crop-mark">{first_name}</span></p>
            </li>
        );
    }
}


TaskElement.PropTypes = {
    id: PropTypes.number.isRequired,
    status: PropTypes.bool,
    name: PropTypes.string,
    description: PropTypes.string,
    created: PropTypes.string,
    first_name: PropTypes.string
};
