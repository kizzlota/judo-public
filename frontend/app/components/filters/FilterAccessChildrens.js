import React, {Component} from 'react';
import {GLOBAL} from '../../../config';
import is from 'is_js';
import {connect} from 'react-redux';
import {PUBLICK_STATUS, PROTECTED_STATUS} from '../../const/PageInfo';


class FilterAccessChildrens extends Component {
    constructor(props) {
        super(props);
    }

    getAccessCildren(element) {
        let {access_status} = element.type;
        return is.equal(PROTECTED_STATUS, access_status);
    }
    filterChildrenfromaccess() {
        let {children, statusOpen} = this.props;
        if (is.array(children)) {
            return children.filter((child) => {
                let access_status = this.getAccessCildren(child);
                return statusOpen === access_status || !access_status ? child : false;
            });
        }
        let access_status = this.getAccessCildren(children);
        return statusOpen === access_status || !access_status ? children : null;
    }

    render() {
        let {children, statusOpen} = this.props;
        let filter_el = !statusOpen ? this.filterChildrenfromaccess(children) : children;
        return (
            filter_el
        );
    }
}

function mapStateToProps(state) {
    return {
        statusOpen: state.userData.statusOpen
    };
}

export default connect(mapStateToProps)(FilterAccessChildrens);
