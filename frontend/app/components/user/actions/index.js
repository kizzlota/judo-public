import {OPEN_STATUS, USER_INFO, AUTH_TRUE} from '../../../const/PageInfo';

export let opensStatus = (status) => {
    return {type: OPEN_STATUS, statusOpen: status};
};
export let userInfo = (status) => {
    return {type: USER_INFO, userInfo: status};
};
export let logoutAction = () => {
    return {type: AUTH_TRUE, userInfo: {}, statusOpen: false};
};

export let authSuccess = (data) => {
    let {userInfo: info, statusOpen} = data;
    return {type: AUTH_TRUE, userInfo: info, statusOpen};
};
