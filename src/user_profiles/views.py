from django.views.generic import TemplateView
from user_profiles.forms import AcountAuthorizationForm
from django.views import View
from django.shortcuts import  redirect
from django.contrib.auth import authenticate, login, logout

from tasks.models import Task
from django.conf import settings
from django.urls import reverse

from django.contrib import messages
import emoji

# Create your views here.

from django.shortcuts import render


def error_403_view(request):
    return render(request, '403.html')


def error_404_view(request):
    return render(request, '404.html')


def error_500_view(request):
    return render(request, '500.html')

def todoses(request):
    return render(request=request, template_name='index.html')

class BasicView(TemplateView):
    template_name = 'base.html'

    def get(self, request, *args, **kwargs):
        react = settings.USE_REACT_JS
        tasks = Task.objects.all().order_by('id').reverse()
        if react:
            return render(request=request, template_name='index.html')
        else:
            if request.user.is_authenticated:
                return render(request=request, template_name='tasks_tmpl/all_tasks.html',
                              context={'object_list': tasks})
            else:
                return redirect(reverse('login'))

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['everyone_tasks'] = Task.objects.all().order_by('id').reverse()
        return context


class AccountLogin(View):
    _form = AcountAuthorizationForm()

    def get(self, request, *args, **kwargs):
        '''
        well documented comments here )
        :param request:
        :return: redirect to success or raise some error
        '''

        if not request.user.is_authenticated:
            form = self._form
            return render(request, 'account_tmpl/login.html', {'form': form})
        else:
            return redirect('/')

    def post(self, request, *args, **kwargs):
        form = AcountAuthorizationForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data.get('email')
            password = form.cleaned_data.get('password')
            user = authenticate(request, username=email, password=password)
            if user is not None:
                login(request, user=user)
                messages.success(request=self.request, message="{} user successfully logined ".format(
                    emoji.emojize(':ok_hand: :wink: ', use_aliases=True)),
                                 extra_tags='alert alert-danger form-group" role="alert')
                return redirect('/tasks/all')
            else:
                messages.error(request=self.request, message="login failure".format(
                    emoji.emojize(':sob:', use_aliases=True)),
                               extra_tags='alert alert-danger form-group" role="alert')
                form.add_error(field='email', error='invalid email or password')
                return render(request, 'account_tmpl/login.html', {'form': form})
        else:
            form.add_error(field='email', error='invalid email or password')
            return render(request, 'account_tmpl/login.html', {'form': form})


class AccountLogout(View):
    def get(self, request):
        logout(request)
        messages.success(request=self.request, message="{} user successfully logout ".format(
            emoji.emojize(':runner: :dash:', use_aliases=True)),
                         extra_tags='alert alert-danger form-group" role="alert')
        return redirect(reverse('main'))
