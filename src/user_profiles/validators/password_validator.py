from django.core.exceptions import ValidationError


def psswd_validator(value):
    if len(value) < 8:
        raise ValidationError('passwod in can not be shorter than 10 values')
    if not any(val for val in value if val.isalpha() and val.isupper()) and any(val for val in value if val.isdigit()):
        raise ValidationError('passwod must contain upper letters and digits')
    return value
