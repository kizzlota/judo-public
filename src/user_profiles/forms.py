from django import forms
from user_profiles.validators.password_validator import psswd_validator
from django.core.exceptions import ValidationError


class AcountAuthorizationForm(forms.Form):
    email = forms.EmailField(max_length=255,
                             label='username',
                             initial='username',
                             required=True,
                             )
    password = forms.CharField(max_length=255,
                               widget=forms.PasswordInput,
                               label='password',
                               initial='enter a password here',
                               required=True

                               )

    email.widget.attrs.update({'class': 'col-md-6'})
    password.widget.attrs.update({'class': 'col-md-6'})

    def clean_email(self):
        '''
        example of validdation values in form exactly. not external validator
        :return:
        '''
        email = self.cleaned_data.get('email')
        if not '@' in email:
            raise ValidationError('email is not in prorep format', code='invalid')
        return email
