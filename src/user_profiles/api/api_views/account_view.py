from django.contrib.auth import authenticate, login, logout
from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework import status
from user_profiles.api.serializers.account_serializer import *


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = get_user_model().objects.all().order_by('-date_joined')
    serializer_class = UserSerializer


class AccountTockenGet(viewsets.ViewSet):
    permission_classes = (AllowAny,)

    def create(self, request):
        user = request.user
        request_token = request.auth
        db_token = Token.objects.get(user=request.user)
        key = db_token.key
        if str(request_token) == str(key):
            if user.is_active:
                data = {}
                data['email'] = db_token.user.email if db_token.user.email else None
                data['firstname'] = db_token.user.first_name if db_token.user.first_name else None
                return Response(data=data, status=status.HTTP_200_OK)
            return Response({'ERROR': 'token is not valid'}, status=status.HTTP_400_BAD_REQUEST)
        return Response({'ERROR': 'token is not valid'}, status=status.HTTP_400_BAD_REQUEST)


class AccountViewSet(viewsets.ViewSet):
    permission_classes = (AllowAny,)

    def create(self, request):
        serializer = AccountSerializer(data=request.data, remove_fields=['password'])
        if serializer.is_valid():
            user_instance = serializer.save()

            email = serializer.validated_data.get('email')

            response = Response(serializer.data, status=status.HTTP_201_CREATED)
            response.set_cookie('email', email, max_age=1800)

            user_instance.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, user=user_instance)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AccountUpdateViewSet(viewsets.ViewSet):
    permission_classes = (IsAuthenticated,)

    def update(self, request):
        try:
            user_instance = User.objects.get(Q(id=request.user.id) | Q(email=request.user))
        except User.DoesNotExist as e:
            return Response({'ERROR': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        else:
            serializer = AccountUpdateSerializer(instance=user_instance, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response(serializer.data, status=status.HTTP_202_ACCEPTED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request):
        try:
            user_instance = User.objects.get(Q(email=request.user.email) | Q(id=request.user.id))
        except User.DoesNotExist as e:
            return Response({'ERROR': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        else:
            serializer = AccountSerializer(instance=user_instance, remove_fields=['password'])
            if serializer:
                return Response(serializer.data, status=status.HTTP_200_OK)
            return Response({"ERROR": "no data in the profile"}, status=status.HTTP_204_NO_CONTENT)


class ApiAccountLogin(viewsets.ViewSet):
    permission_classes = (AllowAny,)

    def login(self, request):
        try:
            User.objects.get(email=request.data.get('email'))
        except User.DoesNotExist:
            return Response({'ERROR': 'user does not exist'}, status=status.HTTP_400_BAD_REQUEST)
        serializer = AccountLoginSerializer(data=request.data)
        if serializer.is_valid():

            username = serializer.validated_data.get('email')
            password = serializer.validated_data.get('password')
            user = authenticate(email=username, password=password)
            if user is not None:
                login(request, user)
                return Response({"email": serializer.data.get('email')}, status=status.HTTP_200_OK)
            else:
                return Response({'ERROR': 'username or password is incorrect.'},
                                status=status.HTTP_400_BAD_REQUEST)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def logout(self, request):
        logout(request)
        return Response(status=status.HTTP_200_OK)
