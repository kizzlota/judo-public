from rest_framework import serializers
from rest_framework.validators import UniqueValidator, ValidationError
from user_profiles.models import *
from django.core import validators
from django.utils.translation import ugettext_lazy as _
import re


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('id', 'url', 'email')  # 'username',


class AccountCreateSerializer(serializers.Serializer):
    query = serializers.CharField(max_length=100, validators=[validators.RegexValidator(re.compile('^[a-zA-Z0-9@+-]')),
                                                              UniqueValidator(queryset=User.objects.all(),
                                                                              message=_(
                                                                                  "user with the same credentials already exists"))])


class AccountSerializer(serializers.ModelSerializer):
    '''
    fields validation - username , email & serializing model User. but User model has been modified and used
    rest-framework, for this reason had used --- get_user_model()
    get_user_model() - locale in Python27\Lib\site-packages\django\contrib\auth\__init__.py
    '''
    email = serializers.CharField(max_length=60, required=True, allow_blank=False,
                                  validators=[
                                      UniqueValidator(queryset=User.objects.all(),
                                                      message=_('User with same Email address already exists.')),
                                      validators.EmailValidator()]
                                  )
    first_name = serializers.CharField(max_length=250, required=False)

    def __init__(self, *args, **kwargs):
        remove_fields = kwargs.pop('remove_fields', None)
        super(AccountSerializer, self).__init__(*args, **kwargs)
        if remove_fields:
            for field in remove_fields:
                self.fields.pop(field)

    def create(self, validated_data):
        user = User.objects.create_user(**validated_data)
        return user

    class Meta:
        model = get_user_model()
        fields = ('id', 'email', 'password', 'first_name', 'second_name', 'last_name',)

    def validate_email(self, value):
        if isinstance(value, str):
            email_lower = value.lower()
            return email_lower
        else:
            raise ValidationError(_('email needs to be as a string'))

    def to_representation(self, instance):
        # get the original representation
        remove = super(AccountSerializer, self).to_representation(instance)
        # remove fields
        if instance.password:
            try:
                remove.__dict__.pop('password', None)
            except KeyError as e:
                raise KeyError(str(e))
        return remove


class AccountUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = get_user_model()
        fields = ('id', 'first_name', 'second_name', 'last_name')

    def update(self, instance, validated_data):
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.second_name = validated_data.get('second_name', instance.second_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.save()
        return instance


class AccountLoginSerializer(serializers.Serializer):
    '''
    serializer used for validate fields
    -------------------------------------------------------------

    '''
    email = serializers.CharField(max_length=30, validators=[validators.EmailValidator()])

    password = serializers.CharField()


class UserPasswordSerializer(serializers.Serializer):
    password = serializers.CharField(write_only=True)
    new_password = serializers.CharField(write_only=True)

    def validate_password(self, value):
        errors = dict()
        if len(value) < 5:
            raise serializers.ValidationError(_('password is too short, it should contain at least 5 characters'))
        try:
            from django.contrib.auth.password_validation import validate_password
            validated_passs = validate_password(password=value)
        except ValidationError as e:
            errors['password'] = list(e.detail)
        if errors:
            raise serializers.ValidationError(errors)
        return super(UserPasswordSerializer, self).validate(value)

    def validate_new_password(self, value):
        errors = dict()
        if len(value) < 5:
            raise serializers.ValidationError(_('password is too short, it should contain at least 5 characters'))
        try:
            from django.contrib.auth.password_validation import validate_password
            validated_passs = validate_password(password=value)
        except ValidationError as e:
            errors['password'] = list(e.detail)
        if errors:
            raise serializers.ValidationError(errors)
        return super(UserPasswordSerializer, self).validate(value)
