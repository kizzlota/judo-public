from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager
from django.contrib.auth import get_user_model
from django.utils import timezone
from django.core.exceptions import ValidationError
from phonenumber_field.modelfields import PhoneNumberField
from django.utils.text import Truncator
from user_profiles.validators.email_validator import email_validator

from  django.db.models.signals import post_save
from django.dispatch import receiver

from rest_framework.authtoken.models import Token
# Create your models here.


class UserManager(BaseUserManager):
    def create_user(self, email=None, password=None, **extra_fields):
        now = timezone.now()
        if email:
            user = self.model(email=self.normalize_email(email), last_login=now, date_joined=now, **extra_fields)
            user.set_password(password)
            user.save(using=self._db)
            return user
        else:
            raise ValidationError('please check if you have provided the proper credentials')

    def create_superuser(self, email, password, **extra_fields):
        user = self.create_user(email=self.normalize_email(email), password=password, **extra_fields)
        extra_fields.setdefault('is_staff', True)
        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(max_length=255, unique=True, verbose_name='e-mail', null=True, blank=True,
                              validators=[email_validator, ])

    first_name = models.CharField(max_length=30, blank=True, null=True)
    last_name = models.CharField(max_length=30, blank=True, null=True)
    second_name = models.CharField(max_length=30, blank=True, null=True)

    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    date_joined = models.DateTimeField(default=timezone.now)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name']

    objects = UserManager()

    def get_full_name(self):
        full_name = f"{self.first_name}, {self.last_name}"
        return full_name.strip()

    def get_short_name(self):
        return self.first_name

    # def email_user(self, subject, message, from_email=None):
    #    send_mail(subject, message, from_email, [self.email])

    def __str__(self):
        return f'user email: {self.email}'


class UserProfile(models.Model):
    class Meta:
        verbose_name = 'Profile'
        verbose_name_plural = 'User Profiles'

    user = models.OneToOneField(User, blank=True, null=True, on_delete=models.CASCADE)
    date_of_birth = models.DateField(blank=True, null=True)
    phone = PhoneNumberField(blank=True, null=True)
    description = models.TextField(max_length=250, blank=True, null=True)

    def __str__(self):
        return Truncator(self.description).chars(10)


@receiver(post_save, sender=User)
def create_profile(instance, created, **kwargs):
        try:
            Token.objects.get(user=instance)
        except Token.DoesNotExist:
            Token.objects.create(user=instance)
        exists = UserProfile.objects.filter(user=instance).exists()
        if not exists:
            user_profile = UserProfile.objects.create(user=instance)
            user_profile.description = 'feel with just text'
            user_profile.save()