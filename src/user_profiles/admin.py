from django.contrib import admin
from .models import *


# Register your models here.


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ['id', 'email', 'first_name', 'last_name', 'is_staff', 'is_active',
                    'date_joined']
    search_fields = ['id', 'email', 'first_name']
    list_filter = ['id', 'email']
    list_editable = ['first_name', 'last_name']

    def save_model(self, request, obj, form, change):
        if obj.pk:
            orig_obj = User.objects.get(pk=obj.pk)
            if obj.password != orig_obj.password:
                obj.set_password(obj.password)
        else:
            obj.set_password(obj.password)
        obj.save()


@admin.register(UserProfile)
class UserProfileAdmin(admin.ModelAdmin):
    list_display = ['id', 'date_of_birth', 'phone', 'description', 'user']
    list_editable = ['phone']
