"""judo_test URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework_swagger.views import get_swagger_view
from rest_framework.authtoken.views import obtain_auth_token
from user_profiles.views import todoses
from django.conf import settings

react = settings.USE_REACT_JS

schema_view = get_swagger_view(title='judo API')

urlpatterns = [
    path('api-token-auth/', obtain_auth_token),
    path('admin/', admin.site.urls),
    path('', include('user_profiles.urls')),
    path('tasks/', include('tasks.urls')),
    path('api/', include('judo_test.api_urls')),
    path('swagger/', schema_view),

]

if react:
    urlpatterns.append(path('todos/', todoses))
