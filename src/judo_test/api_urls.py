from django.urls import path, include
from tasks.api.views.task_view import TaskViewSet
from user_profiles.api.api_views.account_view import AccountViewSet, AccountUpdateViewSet, ApiAccountLogin, \
    AccountTockenGet
from user_profiles.api.api_views.profile_view import ProfileView

api_task_urls = [
    path('create/', TaskViewSet.as_view({'post': 'create'}), name='create_task'),
    path('delete/<id>/', TaskViewSet.as_view({'delete': 'delete'}), name='delete_task'),
    path('retrieve/<id>/', TaskViewSet.as_view({'get': 'retrieve'}), name='retrieve_task'),
    path('update/<id>/', TaskViewSet.as_view({'put': 'update'}), name='update_task'),
    path('all/', TaskViewSet.as_view({'get': 'list'}), name='list_task'),
]

api_user_urls = [
    # user
    path('registration/', AccountViewSet.as_view({'post': 'create'}), name='account_registration'),
    path('token-check/', AccountTockenGet.as_view({'post': 'create'}), name='account_token_heck'),
    path('update/', AccountUpdateViewSet.as_view({'put': 'update'}), name='account_update'),
    path('retrieve/', AccountUpdateViewSet.as_view({'get': 'retrieve'}), name='account_list'),
    path('login/', ApiAccountLogin.as_view({'post': 'login'}), name='user_login'),
    path('logout/', ApiAccountLogin.as_view({'get': 'logout'}), name='user_logout'),
]

api_profile_urls = [
    # profile
    path('create/', ProfileView.as_view({'post': 'create'}), name='create_profile'),
    path('delete/', ProfileView.as_view({'delete': 'delete'}), name='delete_profile'),
    path('retrieve/', ProfileView.as_view({'get': 'retrieve'}), name='retrieve_profile'),
    path('update/', ProfileView.as_view({'put': 'update'}), name='update_profile'),
]

urlpatterns = [

    # api urls
    path('task/', include(api_task_urls)),
    path('account/', include(api_user_urls)),
    path('profile/', include(api_profile_urls)),

]
