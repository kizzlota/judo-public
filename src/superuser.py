#!/usr/bin/env python

import os
import django
from django.contrib.auth.management.commands.createsuperuser import get_user_model

os.environ['DJANGO_SETTINGS_MODULE'] = 'judo_test.settings'
email = os.environ['DJANGO_SU_EMAIL']
db = os.environ['POSTGRES_DB']
passwd = os.environ['DJANGO_SU_PASSWORD']
django.setup()
if get_user_model().objects.filter(email=email, is_superuser=True):
    print('Super user already exists. SKIPPING...')
    # sys.stdout.write('Super user already exists. SKIPPING...')
else:
    print('Creating super user...')
    # sys.stdout.write('Creating super user...')
    # sys.stdout.write((email, passwd))
    print(email, passwd)
    get_user_model()._default_manager.db_manager('default').create_superuser(email=email, password=passwd)
    # sys.stdout.write('Super user created...')
    print('Super user created...')




# import os
# from django.contrib.auth import get_user_model
# def create():
#     User = get_user_model()
#     User.objects.filter(email='admin@admin.com', is_superuser=True).delete()
#     User.objects.create_superuser(email='admin@admin.com', password='admin')
