from rest_framework import serializers
from tasks.models import *


class TaskSerializer(serializers.ModelSerializer):
    first_name = serializers.SerializerMethodField()
    second_name = serializers.SerializerMethodField()
    last_name = serializers.SerializerMethodField()

    class Meta:
        model = Task
        fields = (
            'id', 'name', 'description', 'status', 'author', 'created', 'updated', 'updated_by', 'first_name',
            'second_name', 'last_name',)

    def get_first_name(self, obj):
        try:
            if obj is not None:
                first_name = obj.author.first_name if obj.author else None
                return first_name
        except AttributeError:
            pass

    def get_second_name(self, obj):
        try:
            if obj is not None:
                second_name = obj.author.second_name if obj.author.second_name else None
                return second_name
        except AttributeError:
            pass

    def get_last_name(self, obj):
        try:
            if obj is not None:
                last_name = obj.author.last_name if obj.author.last_name else None
                return last_name
        except AttributeError:
            pass

    def to_representation(self, obj):
        data = super().to_representation(obj)
        if self.context.get('updater_info'):
            try:
                data['updated_by_first_name'] = obj.updated_by.first_name if obj.updated_by.first_name else None
                data['updated_by_last_name'] = obj.updated_by.last_name if obj.updated_by.last_name else None
                data['updated_by_second_name'] = obj.updated_by.second_name if obj.updated_by.second_name else None
            except AttributeError:
                pass
        return data
