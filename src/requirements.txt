attrs==18.1.0
autobahn==18.6.1
Automat==0.7.0
Babel==2.6.0
certifi==2018.4.16
chardet==3.0.4
constantly==15.1.0
coreapi==2.3.3
coreschema==0.0.4
Django==2.0.7
django-channels==0.7.0
django-phonenumber-field==2.0.0
django-rest-framework==0.1.0
django-rest-swagger==2.2.0
djangorestframework==3.8.2
emoji==0.5.0
gunicorn
hyperlink==18.0.0
idna==2.7
incremental==17.5.0
itypes==1.1.0
Jinja2==2.10
MarkupSafe==1.0
oauthlib==2.1.0
openapi-codec==1.3.2
phonenumberslite==8.9.10
Pillow==5.2.0
psycopg2-binary==2.7.5
PyHamcrest==1.9.0
pytz==2018.5
requests==2.19.1
requests-oauthlib==1.0.0
simplejson==3.16.0
six==1.11.0
txaio==2.10.0
uritemplate==3.0.0
urllib3==1.23
zope.interface==4.5.0
django-cors-headers==2.4.0
