# Stage 1 - building artifacts

FROM node:8.4 as build-deps
WORKDIR /usr/src/app
#COPY frontend/package.json frontend/yarn.lock ./
RUN yarn
COPY ./frontend/ ./
COPY ./static/ /usr/src/static
RUN yarn install
RUN yarn build
RUN ls -la /usr/src/app/public/

# Stage 2 - the production environment
FROM python:3.7

RUN apt-get update && apt-get install -y openssh-server \
    && apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false -o APT::AutoRemove::SuggestsImportant=false $buildDeps
COPY --from=build-deps /usr/src/app/public /opt/public
RUN ls -la /opt/public

COPY ./src/requirements.txt ./requirements.txt
RUN pip install -r requirements.txt
